from django.shortcuts import render
from . import models
from atcms import ejson
from django.views.generic.base import View

# Create your views here.
class Index(View):
    def get(self, req):
        env = {}
        env['articles'] = articles = models.Article.objects.order_by('-posted').all()
        return render(req, "audiotech/index.html", env)

class IndexJson(View):
    def get(self, req):
        articles = models.Article.objects.order_by('-posted').all()
        return ejson.response(list(articles))

class Article(View):
    def get(self, req, slug):
        env = {}
        env['article'] = article = models.Article.objects.filter(slug=slug).first()
        return render(req, "audiotech/article.html", env)
