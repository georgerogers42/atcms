from django.apps import AppConfig


class AudiotechConfig(AppConfig):
    name = 'audiotech'
