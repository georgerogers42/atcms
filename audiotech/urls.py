from django.urls import path
from . import views

urlpatterns = [
    path('articles/', views.Index.as_view(), name="audiotech.articles"),
    path('articles/<slug>', views.Article.as_view(), name="audiotech.article"),
    path('api/', views.IndexJson.as_view(), name="audiotech.api"),
    path('', views.Index.as_view(), name="audiotech.home"),
]
