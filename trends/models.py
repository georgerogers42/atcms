from atcms import ejson
from django.db import models
from django.contrib import admin
from django.utils.safestring import mark_safe
from markdown import markdown

# Create your models here.
class Article(models.Model, ejson.Encodable):
    slug = models.CharField(max_length=63, unique=True)
    title = models.CharField(max_length=127, blank=True)
    contents = models.TextField(blank=True)
    posted = models.DateTimeField(blank=False)
    @property
    def md_contents(self):
        return mark_safe(markdown(self.contents))
    def __str__(self):
        return self.slug
    def json_attrs(self):
        return ['id', 'slug', 'title', 'contents', 'posted']

admin.site.register(Article)
