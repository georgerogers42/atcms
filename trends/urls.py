from django.urls import path
from . import views

urlpatterns = [
    path('articles/', views.Index.as_view(), name="trends.articles"),
    path('articles/<slug>', views.Article.as_view(), name="trends.article"),
    path('api/', views.IndexJson.as_view(), name="trends.api"),
    path('', views.Index.as_view(), name="trends.home"),
]
