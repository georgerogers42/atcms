from jinja2 import Environment
from django.urls import reverse

def url(name, mod, *params):
    return reverse(name, mod, params)

def environment(**options):
    env = Environment(**options)
    env.globals.update({'url': url})
    return env
