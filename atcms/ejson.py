import json
import arrow
from django.contrib.auth.models import User, Permission, Group
from django.http import HttpResponse
from datetime import datetime

class Encodable(object):
    def json_attrs(self):
        return []
    def json(self):
        res = {}
        for slot in self.json_attrs():
            res[slot] = getattr(self, slot)
        return res

class Encoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, Encodable):
            return o.json()
        elif isinstance(o, User) or isinstance(o, Permission) or isinstance(o, Group) or isinstance(o, arrow.Arrow):
            return str(o)
        elif isinstance(o, datetime):
            return arrow.get(o)
        else:
            return super(Encoder, self).default(o)

def response(obj, encoder=Encoder()):
    return HttpResponse(encoder.encode(obj), content_type="application/json")
