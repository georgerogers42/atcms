from django.apps import AppConfig


class AtcmsSplashConfig(AppConfig):
    name = 'atcms_splash'
