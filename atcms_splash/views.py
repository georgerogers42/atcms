from django.shortcuts import render
from . import models
from atcms import ejson
from django.views.generic.base import View

class Index(View):
    def get(self, req):
        env = {}
        return render(req, "atcms_splash/index.html", env)
